AdopsiPet
============

[![pipeline status](https://gitlab.com/firdaus.alghifari/adopsipet/badges/master/pipeline.svg)](https://gitlab.com/firdaus.alghifari/adopsipet/commits/master)
[![coverage report](https://gitlab.com/firdaus.alghifari/adopsipet/badges/master/coverage.svg)](https://gitlab.com/firdaus.alghifari/adopsipet/commits/master)
## Nama Kelompok :
- Muhamad Yoga Mahendra (1806205256)
- Inez Amandha Suci (1806141233)
- Firdaus Al-ghifari (1806133780)
- Shannia Dwi Melianti (1806191332)

## Link HerokuApp :
https://adopsipet.herokuapp.com/
atau
http://www.adopsipet.com/


## Deskripsi Aplikasi dan Manfaat : 
Adopsipet adalah platform untuk menghubungkan antara adopter 
dengan orang yang melakukan open adoption. Alasan kami mengambil ide
ini dikarenakan saat ini open adoption di Indonesia lebih sering 
dilakukan lewat media sosial, sehingga kami ingin membuat platform yang
menjadi jembatan untuk mempermudah proses adopsi. Kaitan antara platform
ini dengan Revolusi Industri 4.0 adalah membuat sistem untuk mengawasi dan 
mengakomodasi kebutuhan dari hewan peliharaan. 

Manfaat dari website ini adalah untuk mempermudah adopter dalam menemukan
hewan yang ingin diadopsi, menghubungkan adopter dengan pemilik hewan/shelter, 
mempermudah proses adopsi, serta dapat memonitor hewan peliharaan. 

Daftar fitur :
- Akun User (Register, Login, Setting Profile, My Profile, Logout)
- Search Pet
- Upload Post
- Setting Post (Edit Post or Delete Post)
- About Us
- Contact Us
