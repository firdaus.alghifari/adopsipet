from django.contrib import admin

from .models import Pet, PhotoPet, Profile

admin.site.register(Pet)
admin.site.register(PhotoPet)
admin.site.register(Profile)
