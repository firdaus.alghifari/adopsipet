from django import forms

from .models import Pet

class PetForm(forms.ModelForm):
    class Meta:
        model = Pet
        fields = ['pet_name','pet_type','pet_breed','pet_gender','pet_size','pet_age','pet_health','adoption_fee','description','owner']

class PhotoForm(forms.Form):
    photo = forms.ImageField()
