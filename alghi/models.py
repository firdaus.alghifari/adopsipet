from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    owner_phone = models.CharField(max_length = 50, blank=True)
    owner_country = models.CharField(max_length = 50, blank=True)
    owner_address = models.CharField(max_length = 200, blank=True)
    owner_city = models.CharField(max_length = 50, blank=True)
    owner_zipcode = models.CharField(max_length = 50, blank=True)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

class Pet(models.Model):
    PET_TYPE_CHOICES = [
        ("Cat", "Cat"),
        ("Dog", "Dog"),
        ("Other", "Other")
    ]

    PET_GENDER = [
        ("Male", "Male"),
        ("Female", "Female")
    ]

    PET_SIZE = [
        ("Small", "Small"),
        ("Medium", "Medium"),
        ("Large", "Large"),
        ("XL", "XL")
    ]

    PET_AGE = [
        ("Young", "Young"),
        ("Adult", "Adult"),
        ("Senior", "Senior")
    ]

    pet_name = models.CharField(max_length = 50)
    pet_type = models.CharField(max_length = 50, choices = PET_TYPE_CHOICES)
    pet_breed = models.CharField(max_length = 50)
    pet_gender = models.CharField(max_length = 50, choices = PET_GENDER)
    pet_size = models.CharField(max_length = 50, choices = PET_SIZE)
    pet_age = models.CharField(max_length = 50, choices = PET_AGE)
    pet_health = models.CharField(max_length = 50, default = "")
    adoption_fee = models.CharField(max_length = 50, default = "")
    description = models.TextField(max_length = 500)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

class PhotoPet(models.Model):
    photo = models.ImageField(upload_to='images/')
    pet = models.ForeignKey(Pet, on_delete=models.CASCADE)
