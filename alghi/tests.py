from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile

from .views import upload, category, login_register_view
from .models import Pet

class UploadTest(TestCase):
    def test_upload_url_exists(self):
        response = Client().get('/upload/')
        self.assertEqual(response.status_code, 302)
    
    def test_upload_pet_comprehensive(self):
        client = Client()

        username = 'kocheng'
        password = 'kocheng123'

        # Failed Register - Unmatched Password
        response = client.post('/register/', {
            'username': username,
            'password': password,
            'repeat_password': username,
            'email': username + '@gmail.com',
            'first_name': username,
            'last_name': username,
        })

        user = User.objects.filter(username=username)
        self.assertFalse(user)

        # Failed Register - Invalid Username
        response = client.post('/register/', {
            'username': '',
            'password': password,
            'repeat_password': password,
            'email': username + '@gmail.com',
            'first_name': username,
            'last_name': username,
        })

        user = User.objects.filter(username=username)
        self.assertFalse(user)

        # Register
        response = client.post('/register/', {
            'username': username,
            'password': password,
            'repeat_password': password,
            'email': username + '@gmail.com',
            'first_name': username,
            'last_name': username,
        })

        user = User.objects.filter(username=username)
        self.assertTrue(user)
        user = User.objects.get(username=username)
        
        # Login
        response = client.post('/login/', {
            'username': username,
            'password': password
        })

        # Test Upload URL
        response = client.get('/upload/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'upload.html')

        found = resolve('/upload/')
        self.assertEqual(found.func, upload)

        # Upload something right
        response = client.post('/upload/', {
            "pet_name": "Ame",
            "pet_type": "Cat",
            "pet_breed": "Normal",
            "pet_gender": "Male",
            "pet_size": "Large",
            "pet_age": "Adult",
            "pet_health": "Normal",
            "adoption_fee": "0",
            "description": "Ame is a very awesome cat that understood Depok",
            "owner": user.id,
            "photo": SimpleUploadedFile(name='cute_cat.jpg', content=open("cute_cat.jpg", 'rb').read(), content_type='image/jpeg')
        })

        # Check whether upload is successful
        self.assertIn('Upload successful', response.content.decode())

        # Upload something wrong
        response = client.post('/upload/', {
            "pet_name": "Hohoho"
        })

        # Check whether upload is failed
        self.assertIn('Upload failed', response.content.decode())

        # Logout
        client.get('/logout/')
        
        # Test Logout Successful
        response = client.get('/upload/')
        self.assertEqual(response.status_code, 302)

class CategoryTest(TestCase):
    def test_category_url_exists(self):
        response = Client().get('/category/')
        self.assertEqual(response.status_code, 200)
        
    def test_using_category_view(self):
        found = resolve('/category/')
        self.assertEqual(found.func, category)
    
    def test_using_upload_template(self):
        response = Client().get('/category/')
        self.assertTemplateUsed(response, 'category.html')

    def test_category_filter_animal(self):
        response = Client().post('/category/', {
            'type': 'Cat'
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'category.html')

class LoginTest(TestCase):
    def test_login_register_url_exists(self):
        response = Client().get('/login_register/')
        self.assertEqual(response.status_code, 200)
        
    def test_using_login_view(self):
        found = resolve('/login_register/')
        self.assertEqual(found.func, login_register_view)
    
    def test_using_login_template(self):
        response = Client().get('/login_register/')
        self.assertTemplateUsed(response, 'login.html')

    def test_login_url_exists(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 302)
        
    def test_logout_url_exists(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_wrong_username_message(self):
        response = Client().get('/login_register/?failed')
        self.assertIn('Wrong username/password', response.content.decode())

    def test_need_login_message(self):
        response = Client().get('/login_register/?needlogin')
        self.assertIn('You need to log in first', response.content.decode())

    def test_logged_out_message(self):
        response = Client().get('/login_register/?loggedout')
        self.assertIn('Logout successful', response.content.decode())
