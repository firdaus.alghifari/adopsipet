from django.urls import path

from . import views

urlpatterns = [
    path('upload/', views.upload, name='upload'),
    path('category/', views.category, name='category'),
    path('login_register/', views.login_register_view, name='login_register'),
    path('login/', views.login_view, name='login'),
    path('register/', views.register_view, name='register'),
    path('logout/', views.logout_view, name='logout'),
    path('list_pet/', views.list_pet, name='list_pet'),
    path('list_photo/', views.list_photo, name='list_photo'),
    path('pet_edit/<int:pk>/', views.pet_edit, name='pet_edit'),
]
