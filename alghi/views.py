from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.conf import settings
import copy

from .forms import PetForm, PhotoForm
from .models import Pet, PhotoPet

def upload(request):
    if request.user.is_authenticated:
        pet_form = PetForm()
        if request.method == 'POST':
            newPost = copy.deepcopy(request.POST)
            newPost['owner'] = request.user.id
            pet_form = PetForm(newPost)
            photo_pet_form = PhotoForm(request.POST, request.FILES)
            if pet_form.is_valid() and photo_pet_form.is_valid():
                new_pet = pet_form.save()
                new_photo = PhotoPet(pet = new_pet, photo = photo_pet_form.cleaned_data['photo'])
                new_photo.save()
                return render(request, 'upload.html', {
                    'success': True
                })
            return render(request, 'upload.html', {
                'failed': True
            })
        return render(request, 'upload.html')
    else:
        return redirect('/login_register/?needlogin')

def category(request):
    return render(request, 'category.html')

def login_register_view(request):
    response = {}
    if 'failed' in request.GET:
        response['failed'] = True
    if 'needlogin' in request.GET:
        response['needlogin'] = True
    if 'loggedout' in request.GET:
        response['loggedout'] = True
    return render(request, 'login.html', response)

def login_view(request):
    user = None
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('/')
    else:
        return redirect('/login_register/?failed')

def register_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        password = request.POST['password']
        repeat_password = request.POST['repeat_password']
        if password != repeat_password:
            return redirect('/login_register/?regfailed')
        try:
            user = User.objects.create_user(username, email, password, first_name=first_name, last_name=last_name)
        except:
            return redirect('/login_register/?regfailed')
        user.save()
    return redirect('/')

def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
        return redirect('/login_register/?loggedout')
    else:
        return redirect('/login_register/?needlogin')

def list_pet(request):
    pet_list = list(Pet.objects.values())
    return JsonResponse(pet_list, safe=False)

def list_photo(request):
    photo_list = list(PhotoPet.objects.values())
    for i in range(len(photo_list)):
        photo_list[i]['photo'] = settings.MEDIA_URL + photo_list[i]['photo']
    return JsonResponse(photo_list, safe=False)

def pet_edit(request, pk):
    if request.user.is_authenticated:
        pet = Pet.objects.filter(pk=pk)
        if (pet):
            pet = Pet.objects.get(pk=pk)
            photo_pet = PhotoPet.objects.get(pet=pet.id)
            for key, value in request.POST.items():
                if value != '':
                    if key == 'photo':
                        photo_pet.photo = value
                    else:
                        setattr(pet, key, value)
            pet.save()
            photo_pet.save()
            return render(request, 'pet_edit.html', {
                'pet': pet,
                'photo_pet': photo_pet
            })
    return redirect('/login_register/?needlogin')
