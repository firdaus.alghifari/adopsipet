from django import forms
from .models import Testimonial
from django.forms import ModelForm

class TestimonialForm(ModelForm):
    class Meta:
        model = Testimonial
        fields = ['name', 'occupation', 'testimonial']
        labels = {
            'name' : 'Name', 'occupation' : 'Occupation', 'testimonial' : 'Testimonial'
        }
        widgets = {
            'name' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text'}),
            'occupation' : forms.TextInput(attrs={'class': 'form-control',
                                        'type' : 'text'}),
            'testimonial' : forms.Textarea(attrs={'class': 'form-control',
                                        'type' : 'textarea', 'rows' : '4'}),
        }
