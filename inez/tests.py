from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage, testimonial, postView
from .models import Testimonial

# Create your tests here.
class TestimonialTest(TestCase):
    def test_model_can_create_new_testimonial(self):
        #Creating a new activity
        new_testimonial = Testimonial.objects.create(name='Inez', occupation ='Student', testimonial='Terbantu sekali dengan adanya website ini')

        #Retrieving all available activity
        counting_all_available_testimonial = Testimonial.objects.all().count()
        self.assertEqual(counting_all_available_testimonial,1)

class HomepageTest(TestCase):
    def test_homepage_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_homepage_view(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    def test_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

class PostTest(TestCase):
    def test_post_url_exist(self):
        response = Client().get('/post/')
        self.assertEqual(response.status_code, 302)
    
    def test_using_post_view(self):
        found = resolve('/post/')
        self.assertEqual(found.func, postView)

    '''
    def test_using_post_template(self):
        response = Client().get('/post/')
        self.assertTemplateUsed(response, 'post.html')

    def test_edit_post_url_exist(self):
        response = Client().get('/editpost/<int:pk>/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_edit_post_view(self):
        found = resolve('/editpost/<int:pk>/')
        self.assertEqual(found.func, editPostView)

    def test_using_edit_post_template(self):
        response = Client().get('/editpost/<int:pk>/')
        self.assertTemplateUsed(response, 'edit_post.html')
    

    def test_delete_post_url_exist(self):
        response = Client().get('/deletepost/<int:pk>/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_delete_post_view(self):
        found = resolve('/deletepost/<int:pk>/')
        self.assertEqual(found.func, deletePost)
    '''

    def test_delete_all_post_url_exist(self):
        response = Client().get('/deleteall/')
        self.assertEqual(response.status_code, 302)
    
    '''
    def test_using_delete_all_post_view(self):
        found = resolve('/deleteall/')
        self.assertEqual(found.func, deleteAllPost)
    '''