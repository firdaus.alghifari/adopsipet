from django.urls import path
from . import views

app_name = 'inez'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('testimonial/', views.testimonial, name='testimonial'),
    path('post/', views.postView, name='postView'),
    path('editpost/<int:pk>/', views.editPostView, name='editPostView'),
    path('deletepost/<int:pk>', views.deletePost, name='deletePost'),
    path('deleteall/', views.deleteAllPost, name='deleteAllPost'),
    path('list_pet/', views.list_pet, name='list_pet'),
    path('list_photo/', views.list_photo, name='list_photo'),
    path('list_testimonial/', views.list_testimonial, name='list_testimonial'),
]