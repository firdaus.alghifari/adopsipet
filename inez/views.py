from django.shortcuts import render
from django.shortcuts import redirect
from .forms import TestimonialForm
from .models import Testimonial

from alghi.models import Pet, PhotoPet
from alghi.forms import PetForm

from django.http import JsonResponse
from django.conf import settings

# Create your views here.
def homepage(request):
    data = Testimonial.objects.all()
    pet_list = Pet.objects.all()[:3]
    photo_list = PhotoPet.objects.all()[:3]
    testimonial_count = data.count()

    content = {'title' : 'Testimonials',
                'data' : data,
                'pet_list' : pet_list,
                'photo_list' : photo_list,
                'testimonial_count' : testimonial_count}
    return render(request, 'homepage.html', content)

def testimonial(request):
    if request.method == 'POST':
        form = TestimonialForm(request.POST)
        if form.is_valid():
                post = form.save(commit=False)
                post.save()
                return redirect('inez:homepage')
    else:
        form = TestimonialForm()

    content = {'title' : 'Post Your Testimonial',
                'form' : form}

    return render(request, 'homepage.html', content)
    
def postView(request):
    if request.user.is_authenticated:
        pet_list = Pet.objects.filter(owner = request.user)
        photo_list = PhotoPet.objects.all()
        pet_count = pet_list.count()

        content = {'pet_list' : pet_list,
                    'photo_list' : photo_list,
                    'pet_count' : pet_count}

        return render(request, 'post.html', content)
    else:
        return redirect('/login_register/?needlogin')

def editPostView(request, pk):
    post = Pet.objects.get(pk=pk)
    if request.method == "POST":
        form = PetForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('inez:postView')
    else:
        form = PetForm(instance=post)

    content = {'form' : form,
                'list' : post}
    return render(request, 'edit_post.html', content)

def deletePost(request, pk):
    Pet.objects.filter(pk=pk).delete()
    data = Pet.objects.all()
    content = {'data' : data}
    return redirect('inez:postView')

def deleteAllPost(request):
    if request.method == "POST":
        Pet.objects.all().delete()
        data = Pet.objects.all()
        content = {'data' : data}
        return redirect('inez:postView')
    else:
        return redirect('inez:postView')

def list_pet(request):
    pet_list = list(Pet.objects.values())
    return JsonResponse(pet_list, safe=False)

def list_photo(request):
    photo_list = list(PhotoPet.objects.values())
    for i in range(len(photo_list)):
        photo_list[i]['photo'] = settings.MEDIA_URL + photo_list[i]['photo']
    return JsonResponse(photo_list, safe=False)

def list_testimonial(request):
    testimonial_list = list(Testimonial.objects.values())
    return JsonResponse(testimonial_list, safe=False)
