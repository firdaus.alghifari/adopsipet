from django.db import models
from alghi.models import Pet
from django.contrib.auth.models import User

class AskForPet(models.Model):
    message = models.TextField(max_length = 500, default="some messages.")
    pet = models.ForeignKey(Pet,on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)