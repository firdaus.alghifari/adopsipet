jQuery(document).ready(function($) {
    var v = getParameterID();
    $.ajax({
        url : '/json/askforpet/' + v + '/',
        success: function(result){
            let comments = $('comment');
            comments.empty();
            const daftar_comment = result;
            for(var i=0; i<daftar_comment.length;i++){
                comments.append(
                    '<div class="message-sheet">'+
                    '<div class="comment row">'+
                        '<div class="col-md-4">'+
                            '<h5 class="quest-name">{{ user.first_name }} {{ user.last_name }}</h5>'+
                        '</div>'+
                        '{% if user == owner %}'+
                            '<form action="delete" method="POST" >'+
                            '{% csrf_token %}'+
                                '<input type="hidden" value="'+daftar_comment[i].pk+'" name="id"/>'+
                                '<input class="button-delete delete" type="submit" value="Delete" />'+
                            '</form>'+
                        '{% endif %}'+
                    '</div>'+
                    '<div class="comment row">'+
                        '<div class="col-md-8">'+
                            '<p>'+daftar_comment[i].fields.message+'</p>'+
                        '</div>'+
                    '</div>'+
                '</div>'
                );
            }
        }
    });
});

function getParameterID(){
    var v = alert(window.location.href);
    variable = v.split('/');
    return variable[-1];
};