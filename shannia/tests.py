from django.test import TestCase, Client
from django.urls import resolve
from .views import detail, delete
from .models import AskForPet
from alghi.models import Pet
from django.contrib.auth.models import User

class AskForPetTest(TestCase):
    def test_add_remove_comment(self):
        username = "melisa"
        password = "1234mel"
        my_admin = User.objects.create_user(username=username, password=password)
        my_pet = Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0",description="Ame is a very awesome cat that understood Depok",
            owner=my_admin)
        penanya = User.objects.create_user(username="shannia", password="123456")
        AskForPet.objects.create(message="hallo, mau tanya,apakah ame suka cakar-cakar kursi?",user=penanya, pet=my_pet)
        shannia = AskForPet.objects.filter(user=penanya)
        self.assertTrue(shannia)
        shannia.delete()
        shannia = AskForPet.objects.filter(user=penanya)
        self.assertFalse(shannia)

    def test_post_comment(self):
        username = "melisa"
        password = "1234mel"
        my_admin = User.objects.create_user(username=username, password=password)
        my_pet = Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0",description="Ame is a very awesome cat that understood Depok",
            owner=my_admin)
        penanya = User.objects.create_user(username="shannia", password="123456")
        AskForPet.objects.create(message="hallo, mau tanya,apakah ame suka cakar-cakar kursi?",user=penanya, pet=my_pet)
        response = Client().get('/detail/1/')
        self.assertIn("cakar", response.content.decode())

    def test_not_post(self):
        username = "melisa"
        password = "1234mel"
        my_admin = User.objects.create_user(username=username, password=password)
        my_pet = Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0",description="Ame is a very awesome cat that understood Depok",
            owner=my_admin)
        penanya = User.objects.create_user(username="shannia", password="123456")
        my_pet2 = Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0",description="Ame is a very awesome cat that understood Depok",
            owner=my_admin) 
        AskForPet.objects.create(message="hallo, mau tanya,apakah ame suka cakar-cakar kursi?",user=penanya, pet=my_pet2)
        response = Client().get('/detail/1/')
        self.assertNotIn("bosen", response.content.decode())


class DetailTest(TestCase):
    def test_detail_url_exists(self):
        username = "melisa"
        password = "1234mel"
        my_admin = User.objects.create_user(username=username, password=password)
        Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0",description="Ame is a very awesome cat that understood Depok",
            owner=my_admin)
        response = Client().get('/detail/1/')
        self.assertEqual(response.status_code, 200)

    def test_using_detail_view(self):
        username = "melisa"
        password = "1234mel"
        my_admin = User.objects.create_user(username=username, password=password)
        Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0",description="Ame is a very awesome cat that understood Depok",
            owner=my_admin)
        found = resolve('/detail/1/')
        self.assertEqual(found.func, detail)

    def test_using_detail_template(self):
        username = "melisa"
        password = "1234mel"
        my_admin = User.objects.create_user(username=username, password=password)
        Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0",description="Ame is a very awesome cat that understood Depok",
            owner=my_admin)
        response = Client().get('/detail/1/')
        self.assertTemplateUsed(response, 'detailpost.html')

    def test_post_success(self):
        username = "melisa"
        password = "1234mel"
        my_admin = User.objects.create_user(username=username, password=password)
        Pet.objects.create(pet_name="Ame", pet_type="Cat", pet_breed="Normal", pet_gender="Male", pet_size="Large",
            pet_age="Adult", pet_health="Normal", adoption_fee="0",description="Ame is a very awesome cat that understood Depok",
            owner=my_admin)
        response = Client().get('/detail/1/')
        self.assertIn("Ame", response.content.decode())


    
