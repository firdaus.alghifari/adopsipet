from django.urls import path
from . import views

app_name = "shannia"

urlpatterns = [
    path('detail/<int:detail_id>/', views.detail, name='detail'),
    path('detail/<int:detail_id>/delete', views.delete),
    path('profile/<int:profile_id>/', views.profile, name='profile'),
    path('json/askforpet/', views.json_comment_all),
    path('json/askforpet/<int:detail_id>/', views.json_comment),
]