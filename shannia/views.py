from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import PetAsking
from .models import AskForPet
from alghi.models import Pet, PhotoPet, Profile
from django.contrib.auth.models import User
from alghi.forms import PetForm
from django.http import HttpResponseRedirect, JsonResponse
from django.core import serializers

def detail(request, detail_id):
    detailPet = Pet.objects.get(id=detail_id)
    user = request.user
    photo_list = PhotoPet.objects.all()
    profile = Profile.objects.get(user=detailPet.owner)
    owner = detailPet.owner
    form = PetAsking()
    if request.method =="POST":
        form = PetAsking(request.POST)
        if form.is_valid():
            data = form['message'].value()
            pet_form = AskForPet(message = data, pet=detailPet, user=user)
            pet_form.save()
            return redirect('shannia:detail', detail_id=detail_id)
    else:
        AskForPets = reversed(AskForPet.objects.filter(pet=detailPet))
    return render(request, "detailpost.html", {'detail':detailPet, 'ask': AskForPets,'photo_list':photo_list,'profile':profile, 'owner':owner})

def delete(request, detail_id):
    if request.method == "POST":
        id = request.POST['id']
        AskForPet.objects.get(id=id).delete()
        return redirect('shannia:detail', detail_id=detail_id)

def profile(request, profile_id):
    user = User.objects.get(id=profile_id)
    info = Profile.objects.get(user=user)
    pet = Pet.objects.filter(owner=user)
    photo_list = PhotoPet.objects.all()
    return render(request, "profile.html", {'owner':user,'prof':info, 'pet' : pet, 'photo_list':photo_list})

def json_comment_all(request):
    comment_list=list(AskForPet.objects.values())
    return JsonResponse(comment_list, safe=False)

def json_comment(request, detail_id):
    pet = Pet.objects.get(id=detail_id)
    comment_list = AskForPet.objects.filter(pet=pet)
    data = serializers.serialize('json', comment_list)
    return HttpResponse(data, content_type="application/json")

