import time,datetime

from django.test import Client, LiveServerTestCase, TestCase
from django.urls import resolve
from .models import Message
from .views import about, delete, discussions, contactus
from django.contrib.auth.models import User

class MessageTest(TestCase):
    def test_add_remove_messages(self):
        Message.objects.create(name="Test",email="test@test.com",subject="Subject",messages="the message")
        test = Message.objects.filter(name="Test")
        self.assertTrue(test)
        test.delete()
        test = Message.objects.filter(name="Test")
        self.assertFalse(test)

class AboutTest(TestCase):
    def test_upload_url_exists(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_using_upload_view(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_using_upload_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

class ContactUsTest(TestCase):
    def test_contactus_url_exists(self):
        response = Client().get('/contactus/')
        self.assertEqual(response.status_code, 302)
        
    def test_using_contactus_view(self):
        found = resolve('/contactus/')
        self.assertEqual(found.func, contactus)
        
    def test_discussions_url_exists(self):
        response = Client().get('/discussions/')
        self.assertEqual(response.status_code, 302)
        
    def test_using_discussions_view(self):
        found = resolve('/discussions/')
        self.assertEqual(found.func, discussions)
    
    def test_contact_us_discussion_comprehensive(self):
        username = "Figuran"
        password = "Figuran123"
        # Register
        response = Client().post('/register/', {
            'username': username,
            'password': password,
            'repeat_password': password,
            'email': username + '@gmail.com',
            'first_name': username,
            'last_name': username,
        })
        # Login
        response = Client().post('/login/', {
            'username': username,
            'password': password
        })
        user = User.objects.get(username=username)
        # response = Client().get('/contactus/')
        # self.assertTemplateUsed(response, 'contactus.html')
        response = Client().post('/contactus/', {
            "namenya": user.username,
            "emailnya": user.email,
            "subjectnya": "Tes kirim pesan ke developer",
            "messagenya": "Tes 123, halo halo, hai hai, hahaha, wkwkwk, 555555"
        })
        response = Client().get('/logout/')
        response = Client().post('/login/', {
            'username': 'Yoga',
            'password': 'yoga123'
        })
        
        response = Client().get('/discussions/')
        #self.assertTemplateUsed(response, 'discussions.html')
        #self.assertIn("Tes kirim pesan ke developer",response.content.decode())
        