from django.urls import path
from . import views

urlpatterns = [
    path('about/', views.about, name='about'),
    path('contactus/', views.contactus, name='contactus'),
    path('discussions/', views.discussions, name='discussions'),
    path('discussions/delete', views.delete, name='delete')
]
