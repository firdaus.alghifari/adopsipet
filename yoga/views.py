from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import MessageForm
from .models import Message
from django.utils import timezone
import datetime
import os
# Create your views here.
def about(request):
    return render(request, "about.html")

def discussions(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            messages = Message.objects.order_by("id")
            response = {
                'messages' : messages,
                'server_time' : timezone.now(),
                }
            return render(request, "discussions.html", response)
        else:
            return redirect('/login_register/?needlogin')
    else:
        return redirect('/login_register/?needlogin')

def delete(request):
    if request.user.is_authenticated:
        if request.user.is_superuser:
            if request.method == "POST":
                id = request.POST['id']
                Message.objects.get(id=id).delete()
                return redirect('discussions')
        else:
            return redirect('/login_register/?needlogin')
    else:
        return redirect('/login_register/?needlogin')
    
def contactus(request):
    form = MessageForm()
    if request.user.is_authenticated:
        if request.method =="POST":
            form=MessageForm(request.POST)  # initial={'name': request.user.username,'email':request.user.email}
            if form.is_valid():
                form.save()
                return redirect('contactus') 
            return render(request, "contactus.html", {'form':form})
        else:
            return render(request, "contactus.html", {'form':form})
    else:
        return redirect('/login_register/?needlogin')